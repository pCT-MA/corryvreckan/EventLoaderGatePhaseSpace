#pragma once

#include "core/module/Module.hpp"

class TH2D;
class TFile;
class TTree;

namespace corryvreckan {
    class EventLoaderGatePhaseSpace : public Module {
    public:
        EventLoaderGatePhaseSpace(Configuration& config, std::shared_ptr<Detector> detector);
        virtual ~EventLoaderGatePhaseSpace() {}

        void initialize() override;

        StatusCode run(std::shared_ptr<Clipboard> const&) override;

        void finalize(const std::shared_ptr<ReadonlyClipboard>&) override;

        // magic values
        static constexpr const char* gPhaseSpaceTreeName = "PhaseSpace";
        static constexpr const char* gXBranchName = "X";
        static constexpr const char* gYBranchName = "Y";
        static constexpr const char* gEventIdBranchName = "EventID";
        static constexpr const char* gParentIdBranchName = "ParentID";

    private:
        std::shared_ptr<Detector> mDetector;
        bool mIgnoreDut;
        bool mApplyErrors;
        unsigned mCurrentEvent;
        std::string mPrefix;
        std::string mDataDirectory;
        // input file, root temps
        int mEventId;
        int mParentId;
        float mX;
        float mY;
        long long mCurrentEntry;
        TTree* mTree;
        TFile* mInFile;
        TH2D* mPixelHistogram;
        TH2D* mLocalHistogram;
        TH2D* mGlobalHistogram;
    };
} // namespace corryvreckan
