#include <chrono>
#include <random>

#include "EventLoaderGatePhaseSpace.h"

#include "core/config/exceptions.h"

#include <TFile.h>
#include <TH2D.h>
#include <TSystem.h>
#include <TTree.h>

namespace corryvreckan {
    class CouldNotOpenFileError : public Exception {
    public:
        CouldNotOpenFileError(std::string const& fileName) { error_message_ = "Could not open file: '" + fileName + "'"; }
    };

    class PhaseSpaceTreeNotFoundError : public Exception {
    public:
        PhaseSpaceTreeNotFoundError(std::string const& fileName) {
            error_message_ = "No " + std::string(EventLoaderGatePhaseSpace::gPhaseSpaceTreeName) + " tree found in file: '" +
                             fileName + "'";
        }
    };

    EventLoaderGatePhaseSpace::EventLoaderGatePhaseSpace(Configuration& config, std::shared_ptr<Detector> detector)
        : Module(config, detector), mDetector(detector), mIgnoreDut(false), mApplyErrors(false), mCurrentEvent(0),
          mEventId(0), mParentId(0), mX(0.f), mY(0.f), mCurrentEntry(0), mTree(nullptr), mInFile(nullptr),
          mPixelHistogram(nullptr), mLocalHistogram(nullptr), mGlobalHistogram(nullptr) {
        mIgnoreDut = config.get<bool>("ignore_dut", false);
        mApplyErrors = config.get<bool>("apply_errors", false);
        mPrefix = config.get<std::string>("prefix", "");
        mDataDirectory = config.get<std::string>("data_directory", "");
    }

    void EventLoaderGatePhaseSpace::initialize() {
        if(mIgnoreDut && mDetector->isDUT())
            return;

        auto directory = mDataDirectory;
        if(directory.empty()) {
            /* Workaround to the surprising directory change [here](../../../core/Corryvreckan.cpp:140)
            Strip away the additional `/output`, if it exists, only then will you find the input files */
            directory = std::string(gSystem->pwd());
            if(directory.rfind("/output") != std::string::npos) {
                directory = directory.substr(0, directory.rfind("/output"));
            }
        }

        const auto inFileName = directory + "/" + mPrefix + mDetector->getName() + ".root";
        mInFile = new TFile(inFileName.c_str());
        if(!mInFile->IsOpen())
            throw CouldNotOpenFileError(inFileName);

        mTree = dynamic_cast<TTree*>(mInFile->Get(gPhaseSpaceTreeName));
        if(mTree == nullptr)
            throw PhaseSpaceTreeNotFoundError(inFileName);
        mTree->SetBranchAddress(gXBranchName, &mX, nullptr);
        mTree->SetBranchAddress(gYBranchName, &mY, nullptr);
        mTree->SetBranchAddress(gEventIdBranchName, &mEventId, nullptr);
        mTree->SetBranchAddress(gParentIdBranchName, &mParentId, nullptr);

        // histograms for debugging
        const auto pixels = mDetector->nPixels();
        const auto local = mDetector->getLocalPosition(pixels.X(), pixels.Y());
        const auto global = mDetector->getSize();
        mPixelHistogram = new TH2D("pixel", "Pixel;column;row", pixels.X(), 0, pixels.X(), pixels.Y(), 0, pixels.Y());
        mLocalHistogram = new TH2D("local", "Local;X[mm];Y[mm]", 100, -local.X(), local.X(), 100, -local.Y(), local.Y());
        mGlobalHistogram = new TH2D("global",
                                    "Global;X[mm];Y[mm]",
                                    100,
                                    -0.5 * global.X(),
                                    0.5 * global.X(),
                                    100,
                                    -0.5 * global.Y(),
                                    0.5 * global.Y());
    }

    template <typename T> T GenerateNormal(T mu, T std) {
        static std::default_random_engine engine(
            static_cast<unsigned long>(std::chrono::system_clock::now().time_since_epoch().count()));
        std::normal_distribution<T> distribution(mu, std);
        return distribution(engine);
    }

    StatusCode EventLoaderGatePhaseSpace::run(std::shared_ptr<Clipboard> const& cb) {
        if(mIgnoreDut && mDetector->isDUT())
            return StatusCode::NoData;

        auto readNext = [this]() {
            if(mCurrentEntry + 1 < mTree->GetEntries()) {
                mTree->GetEntry(mCurrentEntry++);
                return true;
            }
            return false;
        };

        if(!cb->isEventDefined()) {
            auto event =
                std::make_shared<Event>(static_cast<double>(mCurrentEvent) - 0.5, static_cast<double>(mCurrentEvent) + 0.5);
            event->addTrigger(mCurrentEvent, static_cast<double>(mCurrentEvent));
            cb->putEvent(event);
        }

        // since we did not load anything yet, do it now
        if(mCurrentEntry == 0 && !readNext())
            return StatusCode::EndRun;

        // skip non-primary hits
        // read up until the events match
        while(mParentId > 0 || static_cast<std::size_t>(mEventId) < mCurrentEvent)
            if(!readNext())
                return StatusCode::EndRun;

        bool didStore = false;
        if(mParentId == 0 && static_cast<std::size_t>(mEventId) == mCurrentEvent) {
            PixelVector resultP;
            ClusterVector resultC;

            const auto x = mX + (mApplyErrors ? GenerateNormal(0., mDetector->getSpatialResolution().X()) : 0.);
            const auto y = mY + (mApplyErrors ? GenerateNormal(0., mDetector->getSpatialResolution().Y()) : 0.);

            const auto local = mDetector->globalToLocal({x, y, mDetector->displacement().Z()});
            const auto row = mDetector->getRow(local);
            const auto col = mDetector->getColumn(local);
            auto pixel = std::make_shared<Pixel>(mDetector->getName(), col, row, 0, 0, mCurrentEvent);
            pixel->setDetectorID(mDetector->getName());
            auto cluster = std::make_shared<Cluster>();
            cluster->setDetectorID(mDetector->getName());
            cluster->setError(mDetector->getSpatialResolution());
            cluster->setClusterCentre({x, y, mDetector->displacement().Z()});
            cluster->setClusterCentreLocal(local);
            cluster->setRow(row);
            cluster->setColumn(col);
            cluster->addPixel(pixel.get());

            if(mDetector->nPixels().X() > col && mDetector->nPixels().Y() > row && mDetector->isWithinROI(cluster.get())) {
                resultC.push_back(cluster);
                resultP.push_back(pixel);
                cb->putData(resultP, mDetector->getName());
                cb->putData(resultC, mDetector->getName());
                mPixelHistogram->Fill(col, row);
                mLocalHistogram->Fill(local.X(), local.Y());
                mGlobalHistogram->Fill(x, y);
                didStore = true;
            }
        }

        if(didStore) {
            readNext();
        }

        ++mCurrentEvent;
        return didStore ? StatusCode::Success : StatusCode::NoData;
    }

    void EventLoaderGatePhaseSpace::finalize(const std::shared_ptr<ReadonlyClipboard>&) {
        if(mPixelHistogram != nullptr)
            mPixelHistogram->Write();
        if(mLocalHistogram != nullptr)
            mLocalHistogram->Write();
        if(mGlobalHistogram != nullptr)
            mGlobalHistogram->Write();
        if(mInFile != nullptr) {
            if(mInFile->IsOpen())
                mInFile->Close();
            delete mInFile;
            mInFile = nullptr;
        }
    }

} // namespace corryvreckan
